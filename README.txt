Quick and dirty utilities for Gephi; for the MOVE tool (a plugin for Gephi): https://github.com/move-tool/gephi-plugin

`jar` can be downloaded here: https://bitbucket.org/neil_rubens/gephi-utilities/raw/eac8dcd4aa1cc7ed631f3659796a95b74982340c/dist/gephi-utilities.jar

It has been upgraded to gephi 0.9 (but hasn't been thoroughly tested).


# Installing

mvn install:install-file -Dfile=gephi-utilities.jar -DgroupId=org.activeintel.citenet -DartifactId=gephi-utils -Dversion=0.2 -Dpackaging=jar


# Etc.

Was developed by using NetBeans 8.1

License: Apache


