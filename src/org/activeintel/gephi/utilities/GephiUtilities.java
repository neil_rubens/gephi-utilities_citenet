/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.gephi.utilities;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import org.gephi.filters.api.FilterController;
import org.gephi.filters.api.Query;
import org.gephi.filters.spi.ComplexFilter;
import org.gephi.filters.spi.FilterProperty;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.GraphView;
import org.gephi.graph.api.Node;
//import org.gephi.graph.api.NodeData;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;
//import org.gephi.graph.dhns.core.Dhns;

/**
 *
 * @author neil
 */
public class GephiUtilities {
    
    
    public static GraphModel importFile(File file) throws FileNotFoundException {
        //Init a project - and therefore a workspace
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        pc.newProject();
        Workspace workspace = pc.getCurrentWorkspace();

        //Get controllers and models
        ImportController importController = Lookup.getDefault().lookup(ImportController.class);
        GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();

        //Import file
        Container container = importController.importFile(file);

        //Append imported data to GraphAPI
        importController.process(container, new DefaultProcessor(), workspace);
        
        return graphModel;
    }
    
    
/*    
    public static Graph copy(Graph graph){
        GraphModel graphModel = graph.getGraphModel();        
        
        FilterController filterController = Lookup.getDefault().lookup(FilterController.class);
        GraphCopyFilter filter = new GraphCopyFilter();
        Query query = filterController.createQuery(filter);
        GraphView view = filterController.filter(query);
        Graph graphCopy = graphModel.getGraph(view);

        return graphCopy;
    }
*/
    
    
    public static Graph copy(Graph graph){
        System.out.println("original graph: nodes #: " + graph.getNodeCount() + " edges #: " + graph.getEdgeCount());
        GraphModel graphModel = graph.getModel();
        GraphView view = graphModel.createView();
        Graph graphCopy = graphModel.getGraph(view);
        graphCopy.addAllNodes(graph.getNodes().toCollection());
        graphCopy.addAllEdges(graph.getEdges().toCollection());        
        System.out.println("graphCopy: nodes #: " + graphCopy.getNodeCount() + " edges #: " + graphCopy.getEdgeCount());
        
        return graphCopy;
    }
    
    
    public static Collection getNodeIds(Graph graph){
        Collection ids = new Vector();
        for ( Node node: graph.getNodes() ){
            ids.add(node.getId());
        }
        return ids;
    }
    


    public static void setNodesColor(org.gephi.graph.api.Graph graph, Color color) {
        Node[] nodes = graph.getNodes().toArray();
        
        for (Node node: nodes){            
            node.setColor(color);
        }
        
        // also set the color of the edges to the same one
        Edge[] edges = graph.getEdges().toArray();
        for (Edge edge: edges){
            edge.setColor(color);
        }
    }
    
    
    public static void setNodesColor(org.gephi.graph.api.Graph graph) {
        setNodesColor(graph, Color.RED);
    }

    
    
    public static void resetNodesColor(org.gephi.graph.api.Graph graph) {
        setNodesColor(graph, new Color(153,153,153));
    }

    
    public static void resetNodesColor(org.gephi.graph.api.Graph graph, Color color) {
        setNodesColor(graph, color);
    }
    
    
    public static String toString(Graph graph){
        StringBuffer strb = new StringBuffer();
        
        strb.append("nodes: " + graph.getNodeCount() + ", edges: " + graph.getEdgeCount());
        
        return strb.toString();
    }
    
    
}
