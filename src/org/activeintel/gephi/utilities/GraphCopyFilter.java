/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.gephi.utilities;

import org.gephi.filters.spi.ComplexFilter;
import org.gephi.filters.spi.FilterProperty;
import org.gephi.graph.api.Graph;

/**
 * Returns a copy of the graph (by not filtering out anything)
 * 
 * @author neil
 */
public class GraphCopyFilter implements ComplexFilter {

    @Override
    public Graph filter(Graph graph) {
        return graph;
    }

    @Override
    public String getName() {
        return "Copy Filter";
    }

    @Override
    public FilterProperty[] getProperties() {
        return null;
    }
    
}

