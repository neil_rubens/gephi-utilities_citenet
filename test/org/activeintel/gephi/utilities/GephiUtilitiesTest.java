/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.gephi.utilities;

import junit.framework.Assert;
import org.gephi.graph.api.Node;
import java.net.URISyntaxException;
import java.io.FileNotFoundException;
import java.io.File;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author neil
 */
public class GephiUtilitiesTest {
    
    public GephiUtilitiesTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of filter method, of class GraphCopyFilter.
     */
    @Test
    public void testCopy() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getResource("/org/activeintel/gephi/utilities/les-miserables.gexf").toURI());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = graphModel.getGraph();
        int nodeNum = graph.getNodeCount();
        int edgeNum = graph.getEdgeCount();
        
        // Make Copy
        Graph graphCopy = GephiUtilities.copy(graph);
        System.out.println("graphCopy: " + graphCopy);
        // Remove nodes from Copy
        Node[] nodes = graphCopy.getNodes().toArray();
        System.out.println("graphCopy nodes #: " + graphCopy.getNodes().toArray().length);
        for (int i = 0; i < 3; i++){
            graphCopy.removeNode(nodes[i]);
        }
        
        // Test that the original graph is not changed
        Assert.assertEquals(nodeNum, graph.getNodeCount());
        Assert.assertEquals(edgeNum, graph.getEdgeCount());
    }

    
    /**
     * Load Testing; and garbage collection
     */
    @Test
    public void testCopyThroughput() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getResource("/org/activeintel/gephi/utilities/les-miserables.gexf").toURI());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = graphModel.getGraph();
        
        for (int i = 0; i < 10000; i++){
            GephiUtilities.copy(graph);
        }
        
    }
    
    
    
}
